Gem::Specification.new do |s|
  s.name        = "ogreview"
  s.version     = "0.1.0"
  s.date        = "2018-07-06"
  s.summary     = "Quick Class/Object/Method outlining tool"
  s.description = "Documentation tool for quickly outlining processes and/or\
                   properties relating to variables, methods, or classes in a\
                   variety of languages."
  s.authors     = ["James Vaughan"]
  s.email       = 'dev.jamesvaughan@gmail.com'
  s.files       = ["lib/ogreview.rb"]
  s.homepage    = "https://gitlab.com/jvaughan/ogreview.git"
  s.license     = "GPL-3.0"
end
