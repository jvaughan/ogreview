require 'ogreview'

RSpec.describe Ogreview do
  it "prints \"Hello, world!\"" do
    ogreview = Ogreview.new
    expect(ogreview.helloWorld).to output("Hello, world!").to_stdout
  end
end
